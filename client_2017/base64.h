#pragma once
#include <string>

class base64 {
public:
	base64();
	static inline bool is_base64(unsigned char c);
	static std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len);
	static std::string base64_decode(std::string const& encoded_string);
	~base64();
};