#pragma once

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cpprest\filestream.h>
#include <cpprest\http_client.h>
#include <cpprest\json.h> // JSON

#include "base64.h"

using namespace std;
using namespace web;


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"


#define	MIN(x,y)	((x)>(y) ? (y) : (x))

#define RECV		0				// receive index
#define SEND		1				// send index

void errexit(const char *, ...);
void CALLBACK SendComplete(DWORD, DWORD, LPWSAOVERLAPPED, DWORD);
void CALLBACK RecvComplete(DWORD, DWORD, LPWSAOVERLAPPED, DWORD);

wstring stringToWstring(string s);
string wstringToString(wstring ws);


char   send_buf[DEFAULT_BUFLEN];	// buffer for sending to server
char   recv_buf[DEFAULT_BUFLEN];	// buffer for receiving from server
WSABUF send_wsabuf;					// WSABUF for sends
WSABUF recv_wsabuf;					// WSABUF for receives
int    send_cnt = 0;				// full count of bytes sent to server
int    recv_cnt = 0;				// full count of bytes received from server


// int i = 0;
int j = 0;
int dim = 0;
bool prima_volta = true;
char* buf;

WSAOVERLAPPED Overlapped[2];		// dummy overlapped structures for send and receive

SOCKET ConnectSocket = INVALID_SOCKET;


int __cdecl main(int argc, char **argv)
{
	WSADATA wsaData;

	int		rc;						// return code

	DWORD  send_num;				// dummy number of bytes sent by last send
	DWORD  recv_num;				// dummy number of bytes received by last receive
	DWORD  recv_flags = 0;			// receive flags

	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	// char *sendbuf = "this is a test";
	// char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	// int recvbuflen = DEFAULT_BUFLEN;

	// Validate the parameters
	if (argc != 2) {
		printf("usage: %s server-name\n", argv[0]);
		return 1;
	}

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}

	/*
	// Send an initial buffer
	iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	printf("Bytes Sent: %ld\n", iResult);

	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	} */

	recv_wsabuf.len = sizeof(recv_buf);
	recv_wsabuf.buf = recv_buf;

	rc = WSARecv(ConnectSocket, &recv_wsabuf, 1, &recv_num, &recv_flags, &Overlapped[RECV], (LPWSAOVERLAPPED_COMPLETION_ROUTINE)RecvComplete);
	int error = WSAGetLastError();
	printf("Receiving up to %d bytes:  return=%d, error=%d\n", recv_wsabuf.len, rc, error);
	if (rc == SOCKET_ERROR && error != WSA_IO_PENDING)
		errexit("Receive error: %d\n", error);

	while (1) {
		SleepEx(INFINITE, TRUE);
	}
	
	/* 
	int i = 0;
	int size = 0;
	bool prima_volta = true;
	char* buf;
	// Receive until the peer closes the connection
	do {
		if (prima_volta) {
			iResult = recv(ConnectSocket, (char*)(&size), 4, 0);
			prima_volta = false;
			std::cout << "size: " << size << std::endl;
			buf = new char[size];
		}
		iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			// printf("Bytes received: %d\n", iResult);
			// printf("ciao: %s", recvbuf);
			strncpy(buf + i, recvbuf, iResult);
			i += iResult;
			std::cout << "totale bytes: " << i << std::endl;
			if (i == (dim))
			{
				std::cout << buf << std::endl;
				string s_apps(buf);
				wstring apps(s_apps.length(), L' ');
				copy(s_apps.begin(), s_apps.end(), apps.begin());
				// Deserialization of the json objects array
				json::value deserializedValue = json::value::parse(apps);
				json::array deserializedArray = deserializedValue.as_array();
				int array_size = deserializedArray.size();
				for (auto it = deserializedArray.cbegin(); it != deserializedArray.cend(); it++) {
					json::value v = *(it);
					json::value focus_v = v.at(L"focus");
					json::value nomeProcesso_v = v.at(L"nomeProcesso");
					json::value iconaProcesso_v = v.at(L"iconaProcesso");
					bool focus = focus_v.as_bool();
					wstring nomeProcesso = nomeProcesso_v.as_string();
					wstring encodedIcon_ws = iconaProcesso_v.as_string();
					string encodedIcon = wstringToString(encodedIcon_ws);
					string decodedIcon = base64::base64_decode(encodedIcon);

					wchar_t* path = L"C:\\test.png";
					HANDLE hFile = CreateFile(path, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
					if (!hFile) hr = HRESULT_FROM_WIN32(GetLastError());
					else {
					DWORD written = 0;
					WriteFile(hFile, a.c_str(), cbSize, &written, 0);
					CloseHandle(hFile);
					}


					wstring path_ws = L"C:\\" + nomeProcesso + L".png";
					const wchar_t* path = path_ws.c_str();
					HANDLE hFile = CreateFile(path, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
					if (!hFile) HRESULT hr = HRESULT_FROM_WIN32(GetLastError());
					else {
						DWORD written = 0;
						WriteFile(hFile, decodedIcon.c_str(), decodedIcon.length(), &written, 0);
						CloseHandle(hFile);
					}
					std::wcout << "Focus: " << focus << std::endl;
					std::wcout << "Nome processo: " << nomeProcesso << std::endl;
					std::cout << "Icona processo: " << encodedIcon << std::endl;
				}
			}
		}
		else if (iResult == 0)
			printf("Connection closed\n");
		else
			printf("recv failed with error: %d\n", WSAGetLastError());

	} while (iResult > 0);

	*/

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}


wstring stringToWstring(string s) {
	wstring ws(s.length(), L' ');
	copy(s.begin(), s.end(), ws.begin());
	return ws;
}


string wstringToString(wstring ws) {
	string s(ws.length(), ' ');
	copy(ws.begin(), ws.end(), s.begin());
	return s;
}



/*
Send completion routine

This routine is called when the overlapped send completes.  The
cbTransferred parameter specifies the number of bytes actually
sent.

The routine does another send if there is more data to send.
*/
/* void CALLBACK SendComplete(DWORD dwError, DWORD cbTransferred, LPWSAOVERLAPPED lpOverlapped, DWORD dwFlags)
{
	int rc;							// return code
	DWORD  send_num;				// dummy number of bytes sent

	if (dwError != 0)
		errexit("Send completion error: %d\n", WSAGetLastError());

	send_cnt += cbTransferred;
	printf("Send completed: %d (total %d)\n", cbTransferred, send_cnt);

	if (send_cnt < SIZEDATA) {
		send_wsabuf.len = MIN(sizeof(send_buf), (SIZEDATA - send_cnt));
		rc = WSASend(sock, &send_wsabuf, 1, &send_num, 0, &Overlapped[SEND], (LPWSAOVERLAPPED_COMPLETION_ROUTINE)SendComplete);
		if (rc == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING)
			errexit("Send call error: %d\n", WSAGetLastError());
		printf("Sending %d bytes:  return=%d, error=%d\n", send_wsabuf.len, rc, WSAGetLastError());
	}

} // SendComplete()
*/



  /*
  Receive completion routine

  This routine is called when the overlapped receive completes.  The
  cbTransferred parameter specifies the number of bytes actually
  received.

  The routine does another receive if not all data is already received.
  */
void CALLBACK RecvComplete(DWORD dwError, DWORD cbTransferred, LPWSAOVERLAPPED lpOverlapped, DWORD dwFlags)
{
	int rc;							// return code
	DWORD  recv_num;				// dummy number of bytes received
	DWORD  recv_flags = 0;			// receive flags

	if (dwError != 0)
		errexit("Receive completion error: %d\n", WSAGetLastError());

	// recv_cnt += cbTransferred;
	// printf("Receive completed: %d (total %d)\n", cbTransferred, recv_cnt);

	if (prima_volta) {
		// recv_cnt += cbTransferred;
		printf("Receive completed: %d (total %d)\n", cbTransferred, cbTransferred);
		// iResult = recv(ConnectSocket, (char*)(&size), 4, 0);
		memcpy(&dim, recv_wsabuf.buf, sizeof(int));
		std::cout << dim << std::endl;
		// copy(&recv_wsabuf.buf[0], &recv_wsabuf.buf[3], &dim);
		//string prova = string(recv_wsabuf.buf, recv_wsabuf.buf+3);
		// int dimensione = stoi(prova);
		prima_volta = false;
		recv_cnt = 0;
		// cout << "size: " << dimensione << std::endl;
		buf = new char[dim];

		recv_wsabuf.len = sizeof(recv_buf);
		recv_wsabuf.buf = recv_buf;

		rc = WSARecv(ConnectSocket, &recv_wsabuf, 1, &recv_num, &recv_flags, &Overlapped[RECV], (LPWSAOVERLAPPED_COMPLETION_ROUTINE)RecvComplete);
		if (rc == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING)
			errexit("Receive call error: %d\n", WSAGetLastError());
		printf("Receiving up to %d bytes:  return=%d, error=%d\n", recv_wsabuf.len, rc, WSAGetLastError());
	}
	else {
		recv_cnt += cbTransferred;
		printf("Receive completed: %d (total %d)\n", cbTransferred, recv_cnt);
		// cout << recv_wsabuf.buf << endl;
		for (int i = 0; i < cbTransferred; i++) {
			buf[j] = recv_wsabuf.buf[i];
			j++;
		}

		if (recv_cnt < dim) {
			rc = WSARecv(ConnectSocket, &recv_wsabuf, 1, &recv_num, &recv_flags, &Overlapped[RECV], (LPWSAOVERLAPPED_COMPLETION_ROUTINE)RecvComplete);
			if (rc == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING)
				errexit("Receive call error: %d\n", WSAGetLastError());
			printf("Receiving up to %d bytes:  return=%d, error=%d\n", recv_wsabuf.len, rc, WSAGetLastError());
		}
		else {
			cout << "Ricezione completata" << endl;
			// cout << buf << endl;

			string s_apps(buf);
			wstring apps = stringToWstring(s_apps);

			// Deserialization of the json objects array
			json::value deserializedValue = json::value::parse(apps);
			json::array deserializedArray = deserializedValue.as_array();
			int array_size = deserializedArray.size();
			for (auto it = deserializedArray.cbegin(); it != deserializedArray.cend(); it++) {
				json::value v = *(it);
				json::value focus_v = v.at(L"focus");
				json::value nomeProcesso_v = v.at(L"nomeProcesso");
				json::value iconaProcesso_v = v.at(L"iconaProcesso");
				bool focus = focus_v.as_bool();
				wstring nomeProcesso = nomeProcesso_v.as_string();
				wstring encodedIcon_ws = iconaProcesso_v.as_string();
				string encodedIcon = wstringToString(encodedIcon_ws);
				string decodedIcon = base64::base64_decode(encodedIcon);

				wstring path_ws = L"C:\\" + nomeProcesso + L".png";
				const wchar_t* path = path_ws.c_str();
				HANDLE hFile = CreateFile(path, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
				if (!hFile) HRESULT hr = HRESULT_FROM_WIN32(GetLastError());
				else {
					DWORD written = 0;
					WriteFile(hFile, decodedIcon.c_str(), decodedIcon.length(), &written, 0);
					CloseHandle(hFile);
				}
				std::wcout << "Focus: " << focus << std::endl;
				std::wcout << "Nome processo: " << nomeProcesso << std::endl;
				std::wcout << "Icona processo: " << encodedIcon_ws << std::endl;
			}

			prima_volta = true;
			dim = 0;
			j = 0;
			delete[] buf;

			rc = WSARecv(ConnectSocket, &recv_wsabuf, 1, &recv_num, &recv_flags, &Overlapped[RECV], (LPWSAOVERLAPPED_COMPLETION_ROUTINE)RecvComplete);
			if (rc == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING)
				errexit("Receive call error: %d\n", WSAGetLastError());
			printf("Receiving up to %d bytes:  return=%d, error=%d\n", recv_wsabuf.len, rc, WSAGetLastError());
		}
	}

} // RecvComplete()



  /*
  Error exit routine as in Comer and Stevens, Vol. III (Winsock edition)
  */
void errexit(const char *format, ...)
{
	va_list	args;

	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	WSACleanup();
	exit(1);

} // errexit()
